
# [LiG Scanner SDK] for Android

## Overview

This SDK provides functionalities for using LigTag device. Users can perform scan operation against 
device and retrieve the transform matrix. This matrix can be applied to the origin anchored by LigTag
precisely.

## Requirements

- Minimum Android API level: 24

## Installation

### Gradle

Add the following to your app's `build.gradle` file:

```gradle
dependencies {
    implementation 'tw.com.lig:scanner:7.2.0'
}
```

### Maven

If you're using Maven, add this to your `pom.xml`:

```xml
<dependency>
  <groupId>tw.com.lig</groupId>
  <artifactId>scanner</artifactId>
  <version>7.2.0</version>
</dependency>
```

## Quick Start

[Provide a simple example to get users started quickly]

```kotlin
// In case mobile phone is not supported by SDK
LiGScanner.onStatusReported = { status ->
    if (status == SdkException.StatusCode.NO_SUPPORT_DEVICE) {
        ...
    }
}


// Initialize the SDK
LiGScanner.initialize(context, "PRODUCT_KEY") // PRODUCT_KEY will be provided after SDK Application is created on LiG Cloud 


// Listen for scanning result
LiGScanner.onLightIDsFound = { ids ->
    ...
}
// Start search for LigTag device
LiGScanner.start()

// Stop searching task
LiGScanner.stop()
```

## Key Concepts

Scanner SDK will provide a `LightID` object which contains information for a given LigTag device.
Devs can draw search-surface for scanning operation with `screenX` and `screenY` fields. Once 
`LightID.isReady` is true, transform matrix can be retrieved via `Transform` instance.

```kotlin
val cameraPose = Transform()
sceneView.arFrame?.camera?.displayOrientedPose?.toMatrix(cameraPose.data, 0)
val transform = lightId.transform(cameraPose)
```

With transform matrix, we can anchor LigTag device onto an `Anchor` object precisely.

```kotlin
val anchor = AnchorNode()
anchor.renderable = renderable
anchor.worldPosition = Vector3(
    transform.translation.x / 1000f,
    transform.translation.y / 1000f,
    transform.translation.z / 1000f
)
anchor.worldRotation = Quaternion(
    transform.rotation.x,
    transform.rotation.y,
    transform.rotation.z,
    transform.rotation.w
)
```

## Main Features

### Positioning for AR World Origin

With LigTag device, we can provide the precise transform matrix for AR World Origin.

### ID Identification

Each LigTag device is unique and secured. Snapshot of LigTag won't work.

## API Reference

[Provide a detailed API reference or link to one]

## Troubleshooting

Frequently Asked Questions
- Ligtag is recommended to be installed onto the wall with 2.2-2.5 meter high.

## Changelog

- 7.2.0

## Support

Mail your problems to rd at lig dot com dot tw

## License

MIT
