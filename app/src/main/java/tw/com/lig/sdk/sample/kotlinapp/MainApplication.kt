package tw.com.lig.sdk.sample.kotlinapp

import android.app.Application
import android.util.Log
import tw.com.lig.sdk.algo.engine.SdkException
import tw.com.lig.sdk.scanner.LiGScanner
import tw.com.lig.sdk.scanner.ScannerStatusListener

class MainApplication: Application() {
    companion object {
        private const val SDK_PRODUCT_KEY = "B62D1-AED87-2C3B0-54A1F-D09D2"
    }

    override fun onCreate() {
        super.onCreate()

        LiGScanner.onStatusReported = { status ->
            if (status == SdkException.StatusCode.NO_SUPPORT_DEVICE) {
                Log.e("LiGScanner", "NO_SUPPORT_DEVICE")
            }
        }
        LiGScanner.initialize(this, SDK_PRODUCT_KEY)
    }

    override fun onTerminate() {
        super.onTerminate()
        LiGScanner.deinitialize()
    }
}