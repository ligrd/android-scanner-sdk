package tw.com.lig.sdk.sample.kotlinapp

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.TextView
import com.google.ar.core.ArCoreApk
import tw.com.lig.sdk.algo.engine.SdkException
import tw.com.lig.sdk.scanner.LiGScanner
import tw.com.lig.sdk.scanner.LightID
import tw.com.lig.sdk.scanner.ScannerStatusListener

class MainActivity: Activity() {

    private var surfaceView: LiGSurfaceView? = null
    private var arAvailability = false
    private var opened = false

    private var arEnabled = false // If you want to test AR, set this to true

    private val sLackPermissions = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private fun updateLightIDMessage(id: LightID) {
        val builder = StringBuilder()

        var status = ""
        when (id.status) {
            LightID.Status.READY -> status = "READY"
            LightID.Status.NOT_DETECTED -> status = "NOT_DETECTED"
            LightID.Status.NOT_DECODED -> status = "NOT_DECODED"
            LightID.Status.INVALID_POSITION -> status = "INVALID_POSITION"
            LightID.Status.NOT_REGISTERED -> status = "NOT_REGISTERED"
            LightID.Status.INVALID_POSITION_TOO_CLOSE -> status = "INVALID_POSITION_TOO_CLOSE"
            LightID.Status.INVALID_POSITION_UNKNOWN -> status = "INVALID_POSITION_UNKNOWN"
        }
        builder.append("Status: $status\n")

        if (id.isDetected) {
            builder.append("Detection: ${id.detectionTime} ms\n")
            builder.append("Decoded: ${id.decodedTime} ms\n")
        }

        if (id.isReady) {
            val locale = resources.configuration.locales.get(0)
            builder.append(String.format(locale, "Rotation: [ %.2f %.2f %.2f ]\n", id.rotation.x, id.rotation.y, id.rotation.z))
            builder.append(String.format(locale, "Translation: [ %.2f %.2f %.2f ]\n", id.translation.x, id.translation.y, id.translation.z))
            builder.append(String.format(locale, "Position:  [ %.2f %.2f %.2f ]\n", id.position.x, id.position.y, id.position.z))
        }

        findViewById<TextView>(R.id.lightid_message).text = builder.toString()
    }

    private fun updateCommandMessage(msg: String) {
        val view = findViewById<TextView>(R.id.command_msg)
        val origin = view.text
        val combine = "${origin}\n${msg}"
        view.text = combine
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQ_CODE) {
            LiGScanner.start()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkARAvailability()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        setContentView(R.layout.activity_main)
        surfaceView = findViewById(R.id.surface_view)
        findViewById<TextView>(R.id.app_version).text = BuildConfig.VERSION_NAME

        // receive scanner status
        LiGScanner.onStatusReported = { status ->
            // If `LiGScanner.start()` is called on the MAIN activity, please execute `start()`
            // when `status` is `ScannerStatusListener.Status.DEVICE_IS_SUPPORTED`.
            if (status == SdkException.StatusCode.DEVICE_IS_SUPPORTED) {
                LiGScanner.start()
            }

            runOnUiThread { updateCommandMessage("onStatus > $status") }
            if (status == SdkException.StatusCode.AUTHENTICATION_OK) {
                val token = LiGScanner.accessToken ?: "N/A"
                runOnUiThread { updateCommandMessage("Access Token(${token.length}) > $token") }
            }
        }

        // receive scan result
        LiGScanner.onLightIDsFound = { ids ->
            if (ids.isNotEmpty()) {
                surfaceView?.let {
                    val lightId = ids[0]
                    it.send(lightId)
                    runOnUiThread {  updateLightIDMessage(lightId) }

                    if (lightId.isReady && !opened && arAvailability && arEnabled) {
                        opened = true
                        val intent = Intent(this, ARActivity::class.java)
                        intent.putExtra("light-id", lightId)
                        startActivity(intent)
                    }
                }
            }
        }

        // show the scanner version and UUID
        updateCommandMessage("UUID > ${LiGScanner.getUUID()}")
        updateCommandMessage("SDK Version > ${LiGScanner.VERSION}")
    }

    private fun checkARAvailability() {
        // @see https://developers.google.com/ar/develop/java/enable-arcore
        ArCoreApk.getInstance().checkAvailabilityAsync(this) { availability ->
            if (availability.isSupported) {
                ArCoreApk.getInstance().requestInstall(this, true)
                arAvailability = true
            } else {
                updateCommandMessage("No ARCore support on this device")
            }
        }
    }

    override fun onStart() {
        super.onStart()
        findViewById<LiGSurfaceView>(R.id.surface_view).startDrawing()
        requestPermissions(sLackPermissions, PERMISSION_REQ_CODE)
    }

    override fun onStop() {
        super.onStop()
        findViewById<LiGSurfaceView>(R.id.surface_view).stopDrawing()

        // stop scanner
        LiGScanner.stop()

        // clear message
        findViewById<TextView>(R.id.command_msg).text = ""

        opened = false
    }

    companion object {
        private const val PERMISSION_REQ_CODE = 10202
    }
}